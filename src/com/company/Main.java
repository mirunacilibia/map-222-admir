package com.company;

import domain.Message;
import domain.validators.*;
import repository.database.FriendshipDBRepository;
import repository.database.MessageDBRepository;
import repository.database.UserDBRepository;
import service.FriendshipService;
import service.MessageService;
import service.SocialNetworkService;
import service.UserService;
import ui.UserInterface;
import ui.UserMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collector;

public class Main {

    public static void main(String[] args) {
        try {
            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            String database = null, password = null;
            switch (args[0]) {
                case "Miruna":
                    database = "jdbc:postgresql://localhost:5432/socialnetwork";
                    password = "postgres";
                    break;
                case "Ada":
                    database = "jdbc:postgresql://localhost:1601/academic";
                    password = "adelamiau";
                    break;
                default:
                    System.out.println("Nu stiu cine esti");
                    return;
            }
            UserDBRepository userDBRepository = new UserDBRepository(database, "postgres",
                    password, new UserValidator(), "users");
            FriendshipDBRepository friendshipDBRepository = new FriendshipDBRepository(database, "postgres",
                    password, new FriendshipValidator(), "friendships");
            MessageDBRepository messageDBRepository = new MessageDBRepository(database, "postgres",
                    password, new MessageValidator(), "messages");

            SocialNetworkService service = new SocialNetworkService(new UserService(userDBRepository),
                    new FriendshipService(friendshipDBRepository), new MessageService(messageDBRepository));

            System.out.println("Login: ");
            String user = read.readLine();
            if (Objects.equals(user, "admin")) {
                UserInterface ui = new UserInterface(service);
                ui.runUI();
            }
            else {
                UserMenu ui = new UserMenu(service,user);
                ui.runUI();
            }
        } catch (IllegalArgumentException | ValidationException | RepositoryException | IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
