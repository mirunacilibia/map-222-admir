package service;

import domain.User;
import domain.validators.RepositoryException;
import domain.validators.ValidationException;
import repository.Repository;

public class UserService implements Service<String, User>{

    Repository<String, User> userRepository;

    /**
     * Cosntructor
     * @param userRepository - the repository of the users
     */
    public UserService(Repository<String, User> userRepository) {
        this.userRepository = userRepository;
    }


    /**
     * @param s - the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     *          or null - if there is no entity with the given id
     * @throws IllegalArgumentException
     *                  if id is null.
     */
    @Override
    public User findOneEntity(String s) {
        return userRepository.findOne(s);
    }

    /**
     * @return all entities
     */
    @Override
    public Iterable<User> findAllEntities() {
        return userRepository.findAll();
    }

    /**
     * @param user - the entity that will be stored
     * @throws ValidationException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.
     * @throws RepositoryException
     *             if the user has already been saved in the memory
     */
    @Override
    public void saveEntity(User user) {
        if(userRepository.save(user) != null)
            throw new RepositoryException("This user is already saved!");
    }

    /**
     *  removes the entity with the specified id
     * @param s
     *      id must be not null
     * @throws IllegalArgumentException
     *             if the given id is null.
     * @throws RepositoryException
     *             if there is no user with the email that we want to update
     */
    @Override
    public void deleteEntity(String s) {
        if(userRepository.delete(s) == null)
            throw new RepositoryException("There is no user with this email!");
    }

    /**
     * @param user - the entity that will be stored
     * @throws ValidationException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.
     * @throws RepositoryException
     *             if there is no user with the email that we want to update
     */
    @Override
    public void updateEntity(User user) {
        if(userRepository.update(user) != null)
            throw new RepositoryException("There is no user with this email!");
    }
}
