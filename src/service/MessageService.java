package service;

import domain.Message;
import domain.validators.RepositoryException;
import domain.validators.ValidationException;
import repository.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MessageService implements Service<Integer, Message>{

    Repository<Integer, Message> messageRepository;

    /**
     * Cosntructor
     * @param messageRepository - the repository of the messages
     */
    public MessageService(Repository<Integer, Message> messageRepository) {
        this.messageRepository = messageRepository;
    }

    /**
     * @param integer - the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     *          or null - if there is no entity with the given id
     */
    @Override
    public Message findOneEntity(Integer integer) {
        return messageRepository.findOne(integer);
    }

    /**
     * @return all entities
     */
    @Override
    public Iterable<Message> findAllEntities() {
        return messageRepository.findAll();
    }

    /**
     * @param message - the message that will be stored
     * @throws ValidationException
     *            if the entity is not valid
     * @throws RepositoryException
     *             if the user has already been saved in the memory
     */
    @Override
    public void saveEntity(Message message) {
        if(messageRepository.save(message) != null)
            throw new RepositoryException("This message is already saved!");
    }

    /**
     *  removes the entity with the specified id
     * @param integer
     *      id must be not null
     * @throws RepositoryException
     *             if there is no user with the email that we want to update
     */
    @Override
    public void deleteEntity(Integer integer) {
        if(messageRepository.delete(integer) == null)
            throw new RepositoryException("There is no message with this id!");
    }

    /**
     * @param message - the message that will be stored
     * @throws ValidationException
     *            if the entity is not valid
     * @throws RepositoryException
     *             if there is no user with the email that we want to update
     */
    @Override
    public void updateEntity(Message message) {
        if(messageRepository.save(message) != null)
            throw new RepositoryException("There is no message with this id!");
    }
}
